package  
{
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class CPlayer 
	{
		private var m_nickname;
		
		public function CPlayer() 
		{
			m_nickname = "nickname";
		}
		
		public function setNickname(name:String):void
		{
			m_nickname = name;
		}
		
		public function getNickname():String
		{
			return m_nickname;
		}
	}
}