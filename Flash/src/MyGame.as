﻿package
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import playerio.*;
	
	//[SWF(width="640",height="400",backgroundColor="#000000",frameRate="30")]
	public class MyGame extends MovieClip
	{
		private const gameID:String = "game1-rkqkselnjeyd39xepttg";
		private var player:CPlayer;
		private var nicknameStage:NicknameStage;
		private var chatStage:ChatStage;
		private var numUsers:int;
		private var mainFont:ProetsenOneEmbedded;
		private var numMessages:int;
		private var m_connection:Connection;
		
		function MyGame()
		{
			player = new CPlayer();
			
			createNicknameStage();
			
			mainFont = new ProetsenOneEmbedded();
			
			numMessages = 0;
		}
		
		private function createNicknameStage():void 
		{
			nicknameStage = new NicknameStage();
			addChild(nicknameStage);
			
			initInputNicknameField();
			
			nicknameStage.okButton.addEventListener(MouseEvent.CLICK, onClickOkButton);
			
			nicknameStage.infoPlane.visible = false;
		}
		
		private function initInputNicknameField():void 
		{
			nicknameStage.inputField.maxChars = 10;
			nicknameStage.inputField.restrict = "A-Za-z0-9";
			stage.focus = nicknameStage.inputField;
		}
		
		private function onClickOkButton(e:MouseEvent):void 
		{
			if (nicknameStage.inputField.text.length > 0)
			{
				captureText();
				nicknameStage.inputField.text = "";
				
				PlayerIO.connect(
				stage,								//Referance to stage
				gameID,								//Game id (Get your own at playerio.com)
				"public",							//Connection id, default is public
				player.getNickname(),				//Username
				"",									//User auth. Can be left blank if authentication is disabled on connection
				null,								//Current PartnerPay partner.
				handleConnect,						//Function executed on successful connect
				handleError							//Function executed if we recive an error
				);
				
				//go to chat stage
				initChatStage();
			}
			else
			{
				nicknameStage.infoPlane.visible = true;
				nicknameStage.infoPlane.txt.text = "Type your nickname!";
				
				//show infoPlane 2 seconds
				var myTimer:Timer = new Timer(2000, 1);
				myTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
				myTimer.start();
			}
		}
		
		function onTimerComplete(e:TimerEvent):void
		{
			nicknameStage.infoPlane.txt.text = "";
			nicknameStage.infoPlane.visible = false;
		}
		
		private function initChatStage():void 
		{
			chatStage = new ChatStage();
			removeChild(nicknameStage);
			addChild(chatStage);
			
			chatStage.nicknameField.text = player.getNickname();
			
			chatStage.inputMessage.restrict = "A-Za-z0-9 ";
			stage.focus = chatStage.inputMessage;
			
			chatStage.sendMessageButton.addEventListener(MouseEvent.CLICK, onClickSendMessageButton);
		}
		
		private function onClickSendMessageButton(e:MouseEvent):void
		{
			m_connection.send("newMessage", chatStage.inputMessage.text, player.getNickname());
			
			//if ()
			//{
				//
			//}
			//chatStage.allMessages.appendText("\n" + chatStage.inputMessage.text);
			
			//var textFormat:TextFormat = new TextFormat();
			//textFormat.size = 20;
			//textFormat.align = TextFormatAlign.LEFT;
			//textFormat.font = mainFont.fontName;
//
			//var textMessage:TextField = new TextField();
			//textMessage.defaultTextFormat = textFormat;
			//textMessage.text = chatStage.inputMessage.text;
			//addChild(textMessage);
			//textMessage.border = false;
			//textMessage.wordWrap = true;
			//textMessage.width = 150;
			//textMessage.height = 40;
			//textMessage.x = 100;
			//textMessage.y = 100;

			//trace(chatStage.inputMessage.text);
			chatStage.inputMessage.text = "";
		}
		
		public function captureText():void 
		{
			//myText = nicknameStage.inputField.text;
			player.setNickname(nicknameStage.inputField.text);
		}
		
		private function handleConnect(client:Client):void
		{
			//trace("Sucessfully connected to player.io");
			
			//Set developmentsever (Comment out to connect to your server online)
			//client.multiplayer.developmentServer = "localhost:8184";
			
			//Create pr join the room test
			client.multiplayer.createJoinRoom(
				"test",								//Room id. If set to null a random roomid is used
				"MyCode",							//The game type started on the server
				true,								//Should the room be visible in the lobby?
				{},									//Room data. This data is returned to lobby list. Variabels can be modifed on the server
				{},									//User join data
				handleJoin,							//Function executed on successful joining of the room
				handleError							//Function executed if we got a join error
			);
		}
		
		
		private function handleJoin(connection:Connection):void{
			//trace("Sucessfully connected to the multiplayer server");
			//gotoAndStop(2);
			 
			m_connection = connection;
			
			//Add disconnect listener
			connection.addDisconnectHandler(handleDisconnect);
					
			/*//Add listener for messages of the type "hello"
			connection.addMessageHandler("hello", function(m:Message){
				trace("Recived a message with the type hello from the server");			 
			})
			
			//Add message listener for users joining the room
			connection.addMessageHandler("UserJoined", function(m:Message, userid:uint){
				trace("Player with the userid", userid, "just joined the room");
			})
			
			//Add message listener for users leaving the room
			connection.addMessageHandler("UserLeft", function(m:Message, userid:uint){
				trace("Player with the userid", userid, "just left the room");
			})*/
			
			//Listen to all messages using a private function
			connection.addMessageHandler("*", handleMessages);
		}
		
		private function handleMessages(m:Message){
			
			switch(m.type)
			{
				case "goToMenu":
					removeChild(chatStage);
					addChild(nicknameStage);
					nicknameStage.infoPlane.txt.text = "Another user have this nickname!";
					nicknameStage.infoPlane.visible = true;
					//show infoPlane 2 seconds
					var myTimer:Timer = new Timer(3000, 1);
					myTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
					myTimer.start();
					break;
				case "newUser":
					//add new people to the list of peoples
					var newUser:PeopleInChat = new PeopleInChat();
					newUser.txt.text = m.getString(0);
					newUser.x = 800;
					trace("numUsers " + (numUsers - 1));
					newUser.y = 80 + (numUsers - 1) * (newUser.height + 10);
					chatStage.addChild(newUser);
					break;
				case "numUsers":
					numUsers = m.getInt(0);
					break;
				case "newMessage":
					//display message
					if(numMessages == 0)
					{
						chatStage.allMessages.text = m.getString(1) + " >> " + m.getString(0);
					}
					else
					{
						chatStage.allMessages.appendText("\n" +  m.getString(1) + " >> " + m.getString(0));
					}
					numMessages++;
					break;
			}
		}
		
		private function handleDisconnect():void{
			trace("Disconnected from server")
		}
		
		private function handleError(error:PlayerIOError):void{
			trace("got", error);
		}
	}	
}
